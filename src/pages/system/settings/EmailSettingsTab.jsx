import React from 'react';
import {Form, Input, Tooltip, Icon, Cascader, Select, Upload, Checkbox, Button, Collapse , Breadcrumb , Tabs , Radio} from 'antd';
const FormItem = Form.Item;
const {TextArea} = Input

class EmailSettingsTab extends React.Component {

    constructor(props){
        super(props);
    }

    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 20},
                sm: {span: 3},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 14},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 14,
                    offset: 6,
                },
            },
        };

        return (
            <div>
                <FormItem {...formItemLayout} label="邮件发送服务器" hasFeedback >
                    {getFieldDecorator('qiniu_access_key', {
                        rules: [{required: true, message: '请填写access key!', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="服务器(SMTP)端口" hasFeedback >
                    {getFieldDecorator('qiniu_secret_key', {
                        rules: [{required: true, message: '请填写secret key!', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="邮箱账号" hasFeedback >
                    {getFieldDecorator('qiniu_bucket_name', {
                        rules: [{required: true, message: '请填写bucket name!', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="邮箱密码" hasFeedback >
                    {getFieldDecorator('qiniu_bucket_host_name', {
                        rules: [{required: true, message: '请填写bucket host name!', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="邮箱测试地址" hasFeedback >
                    {getFieldDecorator('qiniu_bucket_host_name', {
                        rules: [{required: true, message: '请填写bucket host name!', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>
            </div>
        );
    }
}

export default EmailSettingsTab;