import React from 'react';
import {Table , Breadcrumb ,Card , Alert ,Button} from 'antd'
import SearchForm from './list/SearchForm'
import SearchList from './list/SearchList'
import { Link } from 'react-router-dom';
import 'whatwg-fetch'

class CouponListPage extends React.Component {

	state = {
	    data: [{
		  key: '1',
		  name: 'John Brown',
		  age: 32,
		  address: 'New York No. 1 Lake Park',
		}, {
		  key: '2',
		  name: 'Jim Green',
		  age: 42,
		  address: 'London No. 1 Lake Park',
		}, {
		  key: '3',
		  name: 'Joe Black',
		  age: 32,
		  address: 'Sidney No. 1 Lake Park',
		}, {
		  key: '4',
		  name: 'Disabled User',
		  age: 99,
		  address: 'Sidney No. 1 Lake Park',
		}],
	    pagination: {},
	    loading: false,
	    selectedRowKeys : []
	};

	constructor(props) {
		super(props);
	}

	handleTableChange = (pagination, filters, sorter) => {
		this.fetchData({pagination , filters , sorter});
	}

	changeLoading = (loading) => {
		this.setState({loading})
	}

	fetchData = (params = {}) => {
		this.setState({loading:true});
		var self = this;
		fetch('' , params).then((response) => {
			setTimeout(function(){
				self.setState({
					loading : false,
					data : response.data
				})
			} , 1000)
			
		})
	}

	componentDidMount(){
		this.fetchData();
	}

	receiveData = (data) => {
		this.setState(data);
	}

	onSelectChange = (selectedRowKeys) => {
		console.info(selectedRowKeys)
		this.setState({selectedRowKeys})
	} 

	render () {
		return (
			<div id="App-product-list">
				<Breadcrumb className="App-breadcrumb">
                    <Breadcrumb.Item>首页</Breadcrumb.Item>
                    <Breadcrumb.Item>优惠券管理</Breadcrumb.Item>
                    <Breadcrumb.Item>优惠券列表</Breadcrumb.Item>
                </Breadcrumb>
				<Card bordered={false}>
					<SearchForm changeLoading={this.changeLoading} receiveData={this.receiveData}/>
					<Button type="primary"><Link to={`/app/coupon/create`}>创建优惠券</Link></Button>
					<SearchList dataSource={this.state.data} loading={this.state.loading}/>
				</Card>
			</div>
			)
	}
}

export default CouponListPage;