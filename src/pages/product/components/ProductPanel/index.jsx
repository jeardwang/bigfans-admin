import React from 'react';
import {Form, Input, Tooltip, Icon, InputNumber, Select, Table, Upload, Checkbox, Button, Card , Breadcrumb , message,Col} from 'antd';
import ProductPicturesWall from './ProductPicturesWall'
import 'whatwg-fetch'
const FormItem = Form.Item;
const {TextArea} = Input

class ProductPanel extends React.Component {

    constructor(props){
        super(props);
    }
    state = {
        categories : []
    }

    onSpecFieldChange(def) {
        var val = def.target.value;
    }

    componentDidMount () {
        this.refreshLabel();
    }

    refreshLabel () {
        let prodIndex = this.props.prodIndex;
        this.props.specs.map((option , index) => {
            let field = this.props.form.getFieldValue(`products[${this.props.prodIndex}][specs][${option.id}]`);
            console.log(field);
        })
    }

    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 20},
                sm: {span: 3},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 14},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 14,
                    offset: 6,
                },
            },
        };

        const {specs} = this.props;
        const specFields = specs.map((option , index) => {
            return (
                <FormItem {...formItemLayout} label={`${option.name}`} hasFeedback key={index}>
                    {getFieldDecorator(`products[${this.props.prodIndex}][specs][${option.id}]`, {
                        rules: [{required: true, message: `请填写${option.name}!`}],
                    })(
                        <Input onChange={this.onSpecFieldChange}/>
                    )}
                </FormItem>
            )
        })

        return (
            <div>
                {specFields}
                <FormItem {...formItemLayout} label="价格" hasFeedback >
                    {getFieldDecorator(`products[${this.props.prodIndex}][price]`, {
                        rules: [{required: true, message: '请填写价格!'}],
                    })(
                        <InputNumber style={{width:'auto'}}/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="库存" hasFeedback>
                    {getFieldDecorator(`products[${this.props.prodIndex}][stock]`, {
                        rules: [{required: true, message: '请填写价格!'}],
                    })(
                        <InputNumber style={{width:'auto'}}/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="重量" hasFeedback>
                    <Col span={6}>
                        {getFieldDecorator(`products[${this.props.prodIndex}][weight]`, {
                            rules: [{required: true, message: '请填写重量!'}],
                        })(
                            <InputNumber min={1} style={{width:'auto'}} />
                        )}
                    </Col>
                    <Col span={6}>
                        <span style={{color:'#f04134'}}>单位为g,用来计算运费</span>
                    </Col>
                </FormItem>
                <FormItem {...formItemLayout} label="图片" hasFeedback>
                    <ProductPicturesWall form={this.props.form} prodIndex={this.props.prodIndex}/>
                </FormItem>
                <FormItem {...formItemLayout}
                  label="销售信息"
                >
                  <Col span={4}>
                    <FormItem label="最新" 
                              labelCol={{ span: 9 }}
                              >
                        {getFieldDecorator(`products[${this.props.prodIndex}][isNew]` , {initialValue:false})(
                            <Checkbox />
                        )}
                    </FormItem>
                  </Col>
                  <Col span={4}>
                    <FormItem label="推荐"
                              labelCol={{ span: 9 }}
                              >
                        {getFieldDecorator(`products[${this.props.prodIndex}][isRec]`, {initialValue:false})(
                            <Checkbox />
                        )}
                    </FormItem>
                  </Col>
                  <Col span={4}>
                    <FormItem label="热卖"
                              labelCol={{ span: 9 }}
                              >
                        {getFieldDecorator(`products[${this.props.prodIndex}][isHot]`, {initialValue:false})(
                            <Checkbox />
                        )}
                    </FormItem>
                  </Col>
                </FormItem>
            </div>
        );
    }
}

export default ProductPanel;