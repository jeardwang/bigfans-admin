import React from 'react';
import { Form, Input, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete, Switch } from 'antd';
import { LineChart, XAxis, Tooltip, CartesianGrid, Line, Legend, YAxis } from 'recharts';
import { Breadcrumb } from 'antd';
import { Card } from 'antd';
import OrderChart from './OrderChart';
import MemberChart from './MemberChart';
const FormItem = Form.Item;

const data = [
    { name: 'Page A', uv: 4000, pv: 2400, amt: 2400 },
    { name: 'Page B', uv: 3000, pv: 1398, amt: 2210 },
    { name: 'Page C', uv: 2000, pv: 9800, amt: 2290 },
    { name: 'Page D', uv: 2780, pv: 3908, amt: 2000 },
    { name: 'Page E', uv: 1890, pv: 4800, amt: 2181 },
    { name: 'Page F', uv: 2390, pv: 3800, amt: 2500 },
    { name: 'Page G', uv: 3490, pv: 4300, amt: 2100 },
];

class StatisticsPage extends React.Component {

    render() {
        return (
            <div>
                <Breadcrumb className="App-breadcrumb">
                    <Breadcrumb.Item>首页</Breadcrumb.Item>
                    <Breadcrumb.Item>统计</Breadcrumb.Item>
                    <Breadcrumb.Item>创建属性</Breadcrumb.Item>
                </Breadcrumb>
                <Card bordered={false} title="月销量图" extra={<a href="#">More</a>} style={{ marginBottom: '30px' }}>
                    <OrderChart/>
                </Card>
                <Card bordered={false} title="月销量图" extra={<a href="#">More</a>} style={{ marginBottom: '30px' }}>
                    <MemberChart/>
                </Card>
            </div>
        );
    }
}

export default StatisticsPage;